package com.prototype.verticalprototype;

import java.util.Observable;
import java.util.Observer;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import com.cuetoclips.R;

/**
 * Will receive messages from device host and will perform intent
 * to a new screen (either GamePlayerActivity or GameHostActivity).
 * 
 * @author kevinnagaoka
 *
 */
public class RoundWaitingActivity extends Activity implements Observer{
    private TextView waitingTextView;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_round_waiting);
        
        this.waitingTextView = (TextView) findViewById(R.id.text_round_waiting);
        Typeface thin = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/roboto_thin.ttf");
        this.waitingTextView.setTypeface(thin);
        
        HandlerAdapter.getMessages().addObserver(this);
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    }
    
    @Override
    public void onBackPressed() {
        // do nothing
    }
    
    @Override
    protected void onDestroy()
    {
    	super.onDestroy();
    	HandlerAdapter.getMessages().deleteObserver(this);
    }

	@Override
	public void update(Observable arg0, Object arg1)
	{
        String jsonRoundMessage = HandlerAdapter.getMessages().getMessage();
        Log.d("RoundWaitingActivity UPDATE", "Received message: " + jsonRoundMessage);
        String[] strs = jsonRoundMessage.split("(?<=\\})(?=\\{)");
		for(String s : strs)
		{
			try
			{
				JSONObject jObject = new JSONObject(s);
				String action = jObject.getString("action");
				Log.d("ROUNDWAITING", s);
				if(action.equals("VIDEO_SELECT"))
				{
					Log.d("ROUNDWAITING", "VIDEO SELECT");
					// see if the player was the winner
					String playerName = jObject.getString("roundWinner");
					if(Player.getName().equals(playerName))
					{
						Player.incrementRoundsWon();
						GameState.setWinnerOfLastRound(Player.getName());
						// probably need to broadcast who won somehow
					}
					else
					{
						String winner = jObject.getString("roundWinner");
						GameState.setWinnerOfLastRound(winner);
					}
					
					Intent intent;
					if(GameState.getCurrentRound() != SettingsModel.getRoundLimit())
        		    {
            	    	intent = new Intent(getApplicationContext(), RoundOverActivity.class);
        		    }
        		    else
        		    {
        		    	intent = new Intent(getApplicationContext(), GameOverActivity.class);
        		    }

					startActivity(intent);
					finish();
				}else if(action.equals("SCORE"))
				{
					Log.d("ROUNDWAITING", "SCORE");

					Score score = new Score(jObject.getString("playerName"), jObject.getString("score"));
					GameState.getScoreList().add(score);
					Log.d("RoundWaitingActivity Score", "Added new score to GameState");
				}
			}catch(JSONException e)
			{
				e.printStackTrace();
			}
		}
	}
}
