package com.prototype.verticalprototype;

import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import com.cuetoclips.R;

public class SettingsActivity extends Activity
{
	private EditText enteredName;
	private Spinner numRoundsSpinner;
	private Spinner timeLimitSpinner;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings_screen);
		this.enteredName = (EditText) findViewById(R.id.nameTextField);
		this.numRoundsSpinner = (Spinner) findViewById(R.id.num_rounds_spinner);
		this.timeLimitSpinner = (Spinner) findViewById(R.id.time_limit_spinner);
		initListeners();
	}

	@Override
	protected void onResume() {
	    super.onResume();
	    List<String> rounds = Arrays.asList(getResources().getStringArray(R.array.num_rounds_string_array));
        List<String> time = Arrays.asList(getResources().getStringArray(R.array.time_limit_string_array));
        
	    this.enteredName.setText(SettingsModel.getPlayerName());
        this.numRoundsSpinner.setSelection(rounds.indexOf(SettingsModel.getRoundLimit() + ""));
        this.timeLimitSpinner.setSelection(time.indexOf(SettingsModel.getSearchTimeLimit() + ""));
	}
	
	private void initListeners()
	{
		this.enteredName.setOnKeyListener(new OnKeyListener()
		{
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event)
			{
				if(keyCode == KeyEvent.KEYCODE_ENTER)
				{
					String name = enteredName.getText().toString();
					SettingsModel.setPlayerName(name);
					Toast toast = Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT);
					toast.show();
					return true;
				}
				return false;
			}
		});
		
		this.numRoundsSpinner.setOnItemSelectedListener (new OnItemSelectedListener (){

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id)
			{
				String item = (String) parent.getItemAtPosition(position);
				SettingsModel.setRoundLimit(Integer.parseInt(item));
				Toast toast = Toast.makeText(getApplicationContext(), item, Toast.LENGTH_SHORT);
				toast.show();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0)
			{
				
			}
		});
		
		this.timeLimitSpinner.setOnItemSelectedListener(new OnItemSelectedListener()
		{

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id)
			{
				String item = (String) parent.getItemAtPosition(position);
				SettingsModel.setSearchTimeLimit(Integer.parseInt(item));
				Toast toast = Toast.makeText(getApplicationContext(), item, Toast.LENGTH_SHORT);
				toast.show();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0)
			{
				
			}
		});
	}
}
