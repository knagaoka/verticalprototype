package com.prototype.verticalprototype;
/**
 * SettingsModel is the logic behind each option in the Settings menu
 *
 */
public class SettingsModel
{
	private static int searchTimeLimit = 1;
	private static int roundLimit = 1;
	private static String playerName = "";
	
	public static void setSearchTimeLimit(int timeLimit)
	{
		searchTimeLimit = timeLimit;
	}
	public static int getSearchTimeLimit()
	{
		return searchTimeLimit;
	}
	public static void setRoundLimit(int rounds)
	{
		roundLimit = rounds;
	}
	public static int getRoundLimit()
	{
		return roundLimit;
	}
	public static void setPlayerName(String name)
	{
		playerName = name;
	}
	public static String getPlayerName()
	{
		return playerName;
	}
	
	
}
