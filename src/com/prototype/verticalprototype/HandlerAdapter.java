package com.prototype.verticalprototype;

import java.util.ArrayList;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

public class HandlerAdapter extends Handler {
	// Debugging
    private static final String TAG = "HandlerAdapter";
    private static final boolean D = true;
    
	// Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    
    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
    
	// Name of the connected device
    private ArrayList<String> mConnectedDeviceNames = null;
    // Array adapter for the conversation thread
    //private ArrayAdapter<String> mConversationArrayAdapter;
    
    private Context context;
    
    private static MessageDump messages = new MessageDump();
    
    public HandlerAdapter(Context context) {
    	super();
    	this.context = context;
    	this.mConnectedDeviceNames = new ArrayList<String>();
    	for (int i = 0; i < 5; i++) {
    		this.mConnectedDeviceNames.add(null);
    	}
    }
    
    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
        case MESSAGE_STATE_CHANGE:
            if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
            switch (msg.arg1) {
            case BluetoothService.STATE_CONNECTED:
                //setStatus(getString(R.string.title_connected, mConnectedDeviceName));

                break;
            case BluetoothService.STATE_CONNECTING:
                //setStatus(R.string.title_connecting);
                break;
            case BluetoothService.STATE_LISTEN:
            case BluetoothService.STATE_NONE:
                //setStatus(R.string.title_not_connected);
                break;
            }
            break;
        case MESSAGE_WRITE:
            byte[] writeBuf = (byte[]) msg.obj;
            // construct a string from the buffer
            String writeMessage = new String(writeBuf);
            // TODO this is the message you send, do we need this??
            break;
        case MESSAGE_READ:
            byte[] readBuf = (byte[]) msg.obj;
            // construct a string from the valid bytes in the buffer
            String readMessage = new String(readBuf, 0, msg.arg1);
            String deviceName = this.mConnectedDeviceNames.get(msg.arg2);
            // TODO do soemthing with received message here
            
            if (Player.getDeviceHost()) {
                Chat.getChatService().broadcast(readBuf);
            }
            
            messages.setMessage(readMessage);
            break;
        case MESSAGE_DEVICE_NAME:
            // save the connected device's name
        	int pos = msg.arg1;
        	String name = msg.getData().getString(DEVICE_NAME);
            mConnectedDeviceNames.set(pos, name);
            Toast.makeText(context, "Connected to "
                           + name, Toast.LENGTH_SHORT).show();
            break;
        case MESSAGE_TOAST:
            Toast.makeText(context, msg.getData().getString(TOAST),
                           Toast.LENGTH_SHORT).show();
            break;
        }
    }

    public static MessageDump getMessages() {
        return messages;
    }

    public static void setMessages(MessageDump messages) {
        HandlerAdapter.messages = messages;
    }
}
