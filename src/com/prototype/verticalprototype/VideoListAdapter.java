package com.prototype.verticalprototype;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class VideoListAdapter extends BaseAdapter {

	private Context context;
	private List<Video> videoList;
	
	public VideoListAdapter(Context context, List<Video> videos) {
		this.context = context;
		this.videoList = videos;
	}
	
	@Override
	public int getCount() {
		return this.videoList.size();
	}

	@Override
	public Object getItem(int position) {
		return this.videoList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		VideoView videoView = null;
		
		if (convertView == null) {
			videoView = new VideoView(this.context, this.videoList.get(position));
		} else {
			videoView = (VideoView) convertView;
		}
		videoView.setVideo(this.videoList.get(position));
		
		return videoView;
	}
	
	public void clearAdapter() {
	    this.videoList.clear();
	    notifyDataSetChanged();
	}

}
