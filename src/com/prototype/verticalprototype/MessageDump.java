package com.prototype.verticalprototype;

import java.util.Observable;

import android.view.View.OnClickListener;

public class MessageDump extends Observable {
    private String message;
    
    public void setMessage(String m) {
        message = m;
        setChanged();
        notifyObservers();
    }
    
    public String getMessage() {
        return message;
    }
}
