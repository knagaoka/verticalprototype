package com.prototype.verticalprototype;

public class Score
{
	private String playerName;
	private String score;
	
	public Score(String playerName, String score)
	{
		this.playerName = playerName;
		this.score = score;
	}
	public void setPlayerName(String playerName)
	{
		this.playerName = playerName;
	}
	public String getPlayerName()
	{
		return playerName;
	}
	public void setScore(String score)
	{
		this.score = score;
	}
	public String getScore()
	{
		return score;
	}
	
	
}
