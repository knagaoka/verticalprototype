package com.prototype.verticalprototype;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.ImageButton;
import com.cuetoclips.R;

/**
 * MainActivity class gets run when the program starts, inflating main_screen.xml
 */
public class MainActivity extends Activity {

	private ImageButton newGameButton, creditsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen);
        this.newGameButton = (ImageButton) findViewById(R.id.newGameButton);
        this.creditsButton = (ImageButton) findViewById(R.id.creditsButton);
        initButtonListeners();
    }

    private void initButtonListeners(){
    	this.newGameButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				android.content.Intent intent = new android.content.Intent(getApplicationContext(), BluetoothActivity.class);
				//android.content.Intent intent = new android.content.Intent(getApplicationContext(), GamePlayerActivity.class);
				startActivity(intent);
			}
		});
    	this.creditsButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				android.content.Intent intent = new android.content.Intent(getApplicationContext(), CreditsActivity.class);
				startActivity(intent);
			}
		});
    }
    
}
