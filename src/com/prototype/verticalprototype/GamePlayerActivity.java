package com.prototype.verticalprototype;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import com.cuetoclips.R;

/**
 * Will ignore any message that comes through. This class will
 * only send messages out to the device/game host.
 * 
 * @author kevinnagaoka
 *
 */
public class GamePlayerActivity extends Activity implements Observer {
    private TextView topicTextView;
	private EditText searchEditText;
	private ImageButton searchButton;
	private ListView searchResultsList;
	
	private VideoListAdapter videoAdapter;
	private ArrayList<Video> videoList;
	
	private final Context context = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.youtube_search_screen);
		
		this.videoList = new ArrayList<Video>();
		this.videoAdapter = new VideoListAdapter(this, videoList);
		
		this.topicTextView = (TextView) findViewById(R.id.topic_text);
		this.searchButton = (ImageButton) findViewById(R.id.search_button);
		this.searchEditText = (EditText) findViewById(R.id.youtube_search_field);
		this.searchResultsList = (ListView) findViewById(R.id.query_results_list);
		this.searchResultsList.setAdapter(videoAdapter);
		
		Typeface regular = Typeface.createFromAsset(context.getAssets(), "fonts/roboto_regular.ttf");
		this.searchEditText.setTypeface(regular);
		
		initListeners();
		
		HandlerAdapter.getMessages().addObserver(this);
		
		this.topicTextView.setText(GameState.getTopic());
	}
	
	@Override
    public void onBackPressed() {
        // do nothing
    }

	private void initListeners() {
		this.searchButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				String query = searchEditText.getText().toString();
				if (query != null || query != "") {
					videoList.clear();
					videoAdapter.clearAdapter();
					search(query);
				}
				
				// hide keyboard after pressing search button
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);
			}
		});
		
		this.searchEditText.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String query = searchEditText.getText().toString();
                    if (query != null || query != "") {
                        videoList.clear();
                        videoAdapter.clearAdapter();
                        search(query);
                    }
                    
                    // hide keyboard after pressing search button
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);
                    
                    return true;
                }
                return false;
            }
		});
		
		this.searchResultsList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String videoId = ((VideoView) view).getVideo().getVideoId();
				Video vid = ((VideoView) view).getVideo();
				
				Intent intent = new Intent(getApplicationContext(), YouTubePlayerDialogActivity.class);
				intent.putExtra("videoId", videoId);
				intent.putExtra("videoTitle", vid.getVideoTitle());
				intent.putExtra("channelTitle", vid.getVideoChannel());
				intent.putExtra("videoDescription", vid.getVideoDescription());
				intent.putExtra("thumbnailUrl", vid.getVideoThumbnailUrl());
				intent.putExtra("isGameHost", "false");
				startActivity(intent);
			}
		});
		
		this.searchResultsList.setOnItemLongClickListener(new OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, int position, long id) {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_choose_video);
                ImageButton dialogOkay = (ImageButton) dialog.findViewById(R.id.dialog_button_submit_okay);
                ImageButton dialogCancel = (ImageButton) dialog.findViewById(R.id.dialog_button_submit_cancel);
                
                dialogOkay.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        
                        VideoView videoView = (VideoView) view;
                        Video video = videoView.getVideo();
                        JSONObject jsonVideo = new JSONObject();
                        try {
                        	jsonVideo.put("action", "VIDEO");
                            jsonVideo.put("videoId", video.getVideoId());
                            jsonVideo.put("videoTitle", video.getVideoTitle());
                            jsonVideo.put("channelTitle", video.getVideoChannel());
                            jsonVideo.put("videoDescription", video.getVideoDescription());
                            jsonVideo.put("thumbnailUrl", video.getVideoThumbnailUrl());
                            jsonVideo.put("videoTimestamp", "0");
                            jsonVideo.put("name", SettingsModel.getPlayerName());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        
                        String jsonifiedVideo = jsonVideo.toString();
                        sendMessage(jsonifiedVideo);
                        
                        Intent intent = new Intent(getApplicationContext(), RoundWaitingActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                dialogCancel.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("vertproto", "clicked cancel");
                        dialog.dismiss();
                    }
                });
                
                dialog.show();
                
                return true;
            }
		});
	}

	/**
	 * Runs the request for the YouTube query in a separate thread.
	 */
	private void search(final String query) {
		new AsyncTask<String, Void, Integer>() {
			String res;
			
			@Override
			protected Integer doInBackground(String... query) {
				int numVids = 0;
				
				this.res = parseSearchResults(getSearchResults(query[0]));

				return numVids;
			}
			
			@Override
			protected void onPostExecute(Integer result) {
				ArrayList<HashMap<String, String>> metadata = parseJsonResults(res);
				for (HashMap<String, String> video : metadata) {
					Video vid = new Video(video.get("videoId"),
							video.get("title"),
							video.get("channel"),
							video.get("description"),
							video.get("thumbnailUrl"));
					videoList.add(vid);
				}
				Log.d("vertproto", "adding videos to view");
				videoAdapter.notifyDataSetChanged();
			}
		}.execute(query);
	}

	/**
	 * Queries YouTube's API endpoint by sending a GET request with the
	 * appropriate parameters.
	 * 
	 * @param query the search query (i.e. "cats")
	 * @return the HttpResponse for the request
	 */
	private HttpResponse getSearchResults(String query) {
		String devKey = DeveloperKey.DEVELOPER_KEY;

		HttpResponse response = null;
		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet();
			Uri uri = new Uri.Builder().scheme("https")
					.authority("www.googleapis.com")
					.path("youtube/v3/search")
					.appendQueryParameter("part", "snippet")
					.appendQueryParameter("maxResults", "20")
					.appendQueryParameter("key", devKey)
					.appendQueryParameter("q", query)
					.build();
			
			request.setURI(new URI(uri.toString()));
			response = client.execute(request);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return response;
	}

	/**
	 * Parses the HttpResponse from the GET request that is used to query
	 * YouTube.
	 * 
	 * @param response the HttpResponse from the request
	 * @return a string of the raw JSON object
	 */
	private String parseSearchResults(HttpResponse response) {
		String results = null;

		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			String line;
			StringBuffer buffer = new StringBuffer();

			while ((line = in.readLine()) != null) {
				buffer.append(line);
			}
			in.close();
			results = buffer.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return results;
	}

	/**
	 * Parses the JSON object that is returned from the YouTube query. Extracts
	 * videoId, video title, channel name, thumbnail url, and description for
	 * each result.
	 * 
	 * @param results the stringified JSON object
	 * @return an arraylist of hashmaps with metadata
	 */
	private ArrayList<HashMap<String, String>> parseJsonResults(String results) {
		ArrayList<HashMap<String, String>> metadata = new ArrayList<HashMap<String, String>>();

		try {
			JSONObject jObject = new JSONObject(results);
			JSONArray jArray = jObject.getJSONArray("items");

			for (int i = 0; i < jArray.length(); i++) {
				HashMap<String, String> video = new HashMap<String, String>();

				// extract top level json objects
				JSONObject top = jArray.getJSONObject(i);
				JSONObject id = top.getJSONObject("id");
				JSONObject snippet = top.getJSONObject("snippet");
				JSONObject thumbnails = snippet.getJSONObject("thumbnails");
				JSONObject thumbnailHighRes = thumbnails.getJSONObject("high");

				// extract video fields and add to hash map
				String videoId = null;
				try {
					// some objects don't have videoId since they are channel results
					videoId = id.getString("videoId");
				} catch (JSONException e) {
					continue;
				}
				video.put("videoId", videoId);
				video.put("title", snippet.getString("title"));
				video.put("description", snippet.getString("description"));
				video.put("channel", snippet.getString("channelTitle"));
				video.put("thumbnailUrl", thumbnailHighRes.getString("url"));

				metadata.add(video);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return metadata;
	}

	/**
     * Sends a message.
     * @param message  A string of text to send.
     */
    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (Chat.getChatService().getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothService to write
            byte[] send = message.getBytes();
            Chat.getChatService().broadcast(send);
        }
    }

    @Override
    public void update(Observable observable, Object object) {
        String message = HandlerAdapter.getMessages().getMessage();
        
        try {
            JSONObject jObjectData = new JSONObject(message);
            String action = jObjectData.getString("action");
            
            if (action.equals("TOPIC_CARD")) {
                String topicCard = jObjectData.getString("topic");
                this.topicTextView.setText(topicCard);
                Deck.removeCard(topicCard);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
