package com.prototype.verticalprototype;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.cuetoclips.R;

/**
 * Host: Sending a message to everyone saying the round is over
 *       special check for if GameHost is the DeviceHost call broadcast, but don't send message
 * Player: Listening/receiving message from host saying round is over
 * 
 * @author kevinnagaoka
 * 
 */
public class RoundOverActivity extends Activity implements Observer
{
	private TextView roundWinner;
	private ImageButton nextRound;
	private ListView scoreListView;
	//private ArrayList<Score> scoreList = new ArrayList<Score>();
	private ScoreListAdapter scoreListAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_round_over);
		this.scoreListView = (ListView)findViewById(R.id.roundOverInnerLayout);
		this.roundWinner = (TextView) findViewById(R.id.roundWinnerText);
		this.nextRound = (ImageButton) findViewById(R.id.nextRoundButton);
		
		initListeners();
    	this.scoreListAdapter = new ScoreListAdapter(this, GameState.getScoreList());
    	this.scoreListView.setAdapter(this.scoreListAdapter);

		//if you are not the game host, then hide the next round button
		if(!Player.getGameHost()){
			this.nextRound.setVisibility(View.GONE);
		}
		
		HandlerAdapter.getMessages().deleteObservers();
		HandlerAdapter.getMessages().addObserver(this);
		
		this.scoreListAdapter.notifyDataSetChanged();
		
		Log.d("RoundOverActivity OnCreate", "New RoundOverActivity Created");
		//send your score to everyone
		sendScore();
    	if(Player.getDeviceHost())
    	{
    		Log.d("RoundOverActivity OnCreate DH", 
					Player.getName() + " = "
					+ Player.getRoundsWon());
    		addNewView(Player.getName(), Player.getRoundsWon() + "");
    	}
    	
    	// Set the text for the winner of the last round
		this.roundWinner.setText(GameState.getWinnerOfLastRound()
					+ " wins Round "
					+ (GameState.getCurrentRound()));
		
        Typeface bold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/roboto_bold.ttf");
        this.roundWinner.setTypeface(bold);
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		//this.scoreList.clear();
		//HandlerAdapter.getMessages().deleteObserver(this);
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		HandlerAdapter.getMessages().deleteObserver(this);
	}
	
	@Override
    public void onBackPressed() {
        // do nothing
    }
	
	private void addNewView(String playerName, String score)
	{
		Log.d("Adding New Score View", "player: " + playerName + ", score: " + score);
		//create a new view to add a new score
		Score newScore = new Score(playerName, score);
		GameState.getScoreList().add(newScore);
		//this.scoreList.add(newScore);
		//ScoreView scoreView = new ScoreView(this, newScore);
		this.scoreListAdapter.notifyDataSetChanged();
		//this.scoreListView.addView(scoreView);
		//setContentView(this.topLayout);
	}
	
	private void sendScore()
	{
	    JSONObject jObjectData = new JSONObject();
	    try
		{
		    jObjectData.put("action", "SCORE");
		    jObjectData.put("playerName", Player.getName());
		    jObjectData.put("score", Player.getRoundsWon());
		}catch(JSONException e)
		{
			e.printStackTrace();
		}
	    String scoreMsg = jObjectData.toString();
        sendMessage(scoreMsg);
	}
	
	private void initListeners(){
		this.nextRound.setOnClickListener(new OnClickListener()
		{
			//only the game host should have the ability to click next round
			@Override
			public void onClick(View v)
			{
				//Advance to next round, broadcast message to everyone
				
				if (Player.getDeviceHost())
				{
					GameState.incrementCurrentRound();
				}
        	    JSONObject jObjectData = new JSONObject();
        	    try
        		{
        		    jObjectData.put("action", "NEXT_ROUND");
        		    jObjectData.put("gameOver", "false");
        		    jObjectData.put("gameWinner", "null");
        		}catch(JSONException e)
        		{
        			e.printStackTrace();
        		}
        		Player.setGameHost(false);
        	    String nextRound = jObjectData.toString();
                sendMessage(nextRound);
                Intent intent = new Intent(getApplicationContext(), GamePlayerActivity.class);
                startActivity(intent);
                GameState.getScoreList().clear();
                finish();
			}
		});
	}

    @Override
    public void update(Observable observable, Object data) {
        // get message from MessageDump and parse
        String jsonRoundMessage = HandlerAdapter.getMessages().getMessage();
        Log.d("RoundOverActivity", "Whole message received: " + jsonRoundMessage);
        String[] strs = jsonRoundMessage.split("(?<=\\})(?=\\{)");
		for(String s : strs)
		{
			try
			{
				JSONObject jObject = new JSONObject(s);
				String action = jObject.getString("action");
				Log.d("ROUNDOVERACTIVITY", s);
				if (action.equals("TOPIC_CARD")) {
	            	String topic = jObject.getString("topic");
	            	GameState.setTopic(topic);
	            	Deck.removeCard(topic);
	            }
				else if(action.equals("SCORE"))
				{	
					Log.d("UPDATE SCORE", jObject.getString("playerName") + " = "
							+ jObject.getString("score"));
					addNewView(jObject.getString("playerName"),
							jObject.getString("score"));
				}
				else if(action.equals("NEXT_ROUND") && !Player.getGameHost())
				{
					Intent intent;
					GameState.incrementCurrentRound();
					int nextHost = GameState.getCurrentRound()
							% GameState.getPlayers();
					// special case for when mod returns 0, next host is the
					// last person to connect
					if(nextHost == 0)
					{
						nextHost = GameState.getPlayers();
					}

					// Player is next host
					if(nextHost == Player.getHostRound())
					{
						Player.setGameHost(true);
						intent = new Intent(getApplicationContext(),
								GameHostActivity.class);
					}else
					{
						Player.setGameHost(false);
						intent = new Intent(getApplicationContext(),
								GamePlayerActivity.class);
					}

					Log.d("RoundOverActivity",
							"Host Set: " + Player.getGameHost());
					startActivity(intent);
					GameState.getScoreList().clear();
					finish();
					
				}
			}catch(JSONException e)
			{
				e.printStackTrace();
			}
		}
    }
    
    /**
     * Sends a message.
     * @param message A string of text to send.
     */
    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (Chat.getChatService().getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            Chat.getChatService().broadcast(send);
        }
    }
}
