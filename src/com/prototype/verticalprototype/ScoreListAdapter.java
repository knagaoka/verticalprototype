package com.prototype.verticalprototype;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class ScoreListAdapter extends BaseAdapter
{
	private Context context;
	private List<Score> scoreList;
	
	public ScoreListAdapter(Context context, List<Score> scoreList)
	{
		this.context = context;
		this.scoreList = scoreList;
	}
	
	@Override
	public int getCount()
	{
		return this.scoreList.size();
	}

	@Override
	public Object getItem(int arg0)
	{
		return this.scoreList.get(arg0);
	}

	@Override
	public long getItemId(int arg0)
	{
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2)
	{
		ScoreView scoreView = null;
		
		if (convertView == null) {
			scoreView = new ScoreView(this.context, this.scoreList.get(position));
		} else {
			scoreView = (ScoreView) convertView;
		}
		
		scoreView.setScore(this.scoreList.get(position));
		return scoreView;
	}

}
