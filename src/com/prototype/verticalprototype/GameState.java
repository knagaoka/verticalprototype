package com.prototype.verticalprototype;

import java.util.ArrayList;
import android.util.Log;

public class GameState {
	private static int currentRound = 1;
	private static int players;
	private static String topic = "";
	private static String winnerOfLastRound;
	private static ArrayList<Score> scoreList = new ArrayList<Score>();

	public static void reset()
	{
		currentRound = 1;
		scoreList.clear();
		winnerOfLastRound = "";
		topic = "";
	}
	
	public static void setTopic(String topic)
	{
		GameState.topic = topic;
	}
	
	public static String getTopic()
	{
		return topic;
	}
	
	public static int getPlayers() 
	{
		return players;
	}

	public static void setPlayers(int players) 
	{
		GameState.players = players;
	}

	public static void incrementCurrentRound()
	{
		GameState.currentRound++;
		Log.d("GameState", "New Round: " + currentRound);
	}

	public static int getCurrentRound()
	{
		return currentRound;
	}
	
	public static void resetCurrentRound()
	{
		GameState.currentRound = 0;
	}
	
	public static void setWinnerOfLastRound(String winnerOfLastRound)
	{
		GameState.winnerOfLastRound = winnerOfLastRound;
	}

	public static String getWinnerOfLastRound()
	{
		return winnerOfLastRound;
	}

	public static void setScoreList(ArrayList<Score> scoreList)
	{
		GameState.scoreList = scoreList;
	}

	public static ArrayList<Score> getScoreList()
	{
		return scoreList;
	}

}