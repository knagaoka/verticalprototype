package com.prototype.verticalprototype;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import com.cuetoclips.R;

public class GameTypeActivity extends Activity
{
	private Button youtubeButton;
	private Button drawModeButton;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_type_screen);
        this.drawModeButton = (Button) findViewById(R.id.drawModeButton);
        this.drawModeButton.setVisibility(View.GONE);
        this.youtubeButton = (Button) findViewById(R.id.youtubeButton);
        initButtonListeners();
	}

   private void initButtonListeners(){
    	this.drawModeButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				android.content.Intent intent = new android.content.Intent(getApplicationContext(), BluetoothActivity.class);
				startActivity(intent);
			}
		});
    	
    	this.youtubeButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				//TODO: start bluetooth intent first instead of youtube search activity
				android.content.Intent intent = new android.content.Intent(getApplicationContext(), BluetoothActivity.class);
				intent.putExtra("topic", "Cats");
				startActivity(intent);
			}
		});
   }
}
