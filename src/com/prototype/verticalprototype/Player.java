package com.prototype.verticalprototype;
/**
 * Player represents a physical game player. A player has a state that they are in, along with a name.
 * They have a unique identifier, and variables to determine if they are currently the game host or the device host
 */
public class Player {
	private static String name = "";
	private static boolean isDeviceHost = false;
	private static boolean isGameHost = false;
	private static int roundsWon = 0;
	private static int hostRound = 0;
	
	public static void reset()
	{
		isDeviceHost = false;
		isGameHost = false;
		roundsWon = 0;
		hostRound = 0;
	}
	
	public static void setDeviceHost(boolean val){
		Player.isDeviceHost = val;
	}
	
	public static boolean getDeviceHost(){
		return isDeviceHost;
	}
	
	public static void setGameHost(boolean val){
		Player.isGameHost = val;
	}
	
	public static boolean getGameHost(){
		return isGameHost;
	}
	
	public static int getRoundsWon(){
		return roundsWon;
	}
	
	public static void setRoundsWon(int num){
		Player.roundsWon = num;
	}
	
	public static void incrementRoundsWon()
	{
		Player.roundsWon++;
	}

	public static void setName(String name)
	{
		Player.name = name;
	}

	public static String getName()
	{
		return name;
	}

	public static void setHostRound(int hostRound)
	{
		Player.hostRound = hostRound;
	}

	public static int getHostRound()
	{
		return hostRound;
	}
}
