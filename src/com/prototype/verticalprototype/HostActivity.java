package com.prototype.verticalprototype;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.cuetoclips.R;

public class HostActivity extends Activity {

	// Debugging
    private static final String TAG = "HostActivity";
    private static final boolean D = true;
    
	// Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    
    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
    
    // Intent request codes
    private static final int REQUEST_ENABLE_BT = 3;
    
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    
    private Button startButton;
    
 // Return Intent extra
    public static String EXTRA_DEVICE_ADDRESS = "device_address";

    // Member fields
    private TextView mConnectedPlayers;
    private int connectedPlayers;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Setup the window
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_host);
		
		this.startButton = (Button) findViewById(R.id.button_start);
		// Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		initButtonListeners();
		
		String deviceName = mBluetoothAdapter.getName();
        if (!deviceName.contains("alpha")) {
        	mBluetoothAdapter.setName(deviceName + "-alpha");
        }
        
		// Set result CANCELED in case the user backs out
        setResult(Activity.RESULT_CANCELED);
        
        // initialize the deck
        Resources res = getResources();
        Deck.setCards(res.getStringArray(R.array.topic_card_strings));

        mConnectedPlayers = (TextView) findViewById(R.id.connected_players);
        connectedPlayers = 0;

        // Register for broadcasts when a device is connected
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED);
        this.registerReceiver(mReceiver, filter);
        
        // Register for broadcasts when a device is disconnected
        filter = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        this.registerReceiver(mReceiver, filter);
        
        Chat.setUpNewChat(new HandlerAdapter(this));
        Chat.setHostActivity(this);
    }
	
	
	@Override
    public void onStart() {
        super.onStart();
        if(D) Log.d(TAG, "++ ON START ++");
        // If BT is not on, request that it be enabled.
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }
    }
	
	@Override
	protected void onResume() {
		if(D) Log.e(TAG, "+ ON RESUME +");
		super.onResume();
		// Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (Chat.getChatService() != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (Chat.getChatService().getState() == BluetoothService.STATE_NONE) {
              // Start the Bluetooth chat services
            	Chat.getChatService().start();
            }
        }
	}
	
	@Override
    protected void onDestroy() {
        super.onDestroy();

        if (Chat.getChatService() != null) Chat.getChatService().stop();
        if(D) Log.e(TAG, "--- ON DESTROY ---");
        
        // Make sure we're not doing discovery anymore
        if (mBluetoothAdapter != null) {
        	mBluetoothAdapter.cancelDiscovery();
        }

        // Unregister broadcast listeners
        this.unregisterReceiver(mReceiver);
    }

	@Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
	
	private void initButtonListeners(){
		this.startButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{   
				GameState.setPlayers(Chat.getChatService().getConnectedDevices() + 1);
				notifyNumPlayers(GameState.getPlayers());
				startGame();
				android.content.Intent intent = new android.content.Intent(getApplicationContext(), GameHostActivity.class);
				startActivity(intent);
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.host, menu);
		return true;
	}

	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.discoverable:
            // Ensure this device is discoverable by others
            ensureDiscoverable();
            return true;
        }
        return false;
    }
	
    private void ensureDiscoverable() {
        if(D) Log.d(TAG, "ensure discoverable");
        if (mBluetoothAdapter.getScanMode() !=
            BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }
    
    /**
     * Sends a message.
     * @param message A string of text to send.
     */
    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (Chat.getChatService().getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            Chat.getChatService().broadcast(send);
        }
    }
    
    /**
     * Sends a JSON object to all the other devices signalling the start of the game
     */
    private void startGame() {
    	JSONObject jObjectData = new JSONObject();
        try {
            jObjectData.put("action", "START_GAME");
            jObjectData.put("roundLimit", SettingsModel.getRoundLimit() + "");
            sendMessage(jObjectData.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Notifies the other devices of the total number of players in the game.
     * @param players
     */
    private void notifyNumPlayers(int players) {
    	JSONObject jObjectData = new JSONObject();
        try {
            jObjectData.put("action", "NUMBER_OF_PLAYERS");
            jObjectData.put("players", players);
            sendMessage(jObjectData.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // The BroadcastReceiver that listens for discovered devices and
    // changes the title when discovery is finished
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            
            if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
            	// Get the BluetoothDevice object from the Intent
            	BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            	// Add the connected device to the list
            	//mConnectedDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
            	mConnectedPlayers.setText(++connectedPlayers + "");
            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
            	// Get the BluetoothDevice object from the Intent
            	BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            	// Remove the connected device to the list
            	//mConnectedDevicesArrayAdapter.remove(device.getName() + "\n" + device.getAddress());
            	int connectedPlayers = Chat.getChatService().getConnectedDevices();
            	mConnectedPlayers.setText(--connectedPlayers + "");
            }
        }
    };
    
}
