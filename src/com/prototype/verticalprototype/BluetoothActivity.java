package com.prototype.verticalprototype;

import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.cuetoclips.R;

public class BluetoothActivity extends Activity
{
	// Debugging
	protected static final String SAVED_NAME = "enteredName";
    private static final String TAG = "BluetoothActivity";
    private static final boolean D = true;
    
	private ImageButton hostGameButton;
	private ImageButton joinGameButton;
	private TextView titleTextView;
	private TextView nameTextView;
	private TextView roundsTextView;
	
	// Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_ENABLE_BT = 3;
    
	private EditText enteredName;
	private Spinner numRoundsSpinner;
    
    private BluetoothAdapter mBluetoothAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bluetooth);
		this.hostGameButton = (ImageButton) findViewById(R.id.hostGameButton);
		this.joinGameButton = (ImageButton) findViewById(R.id.joinGameButton);
		this.enteredName = (EditText) findViewById(R.id.nameTextField);
		this.numRoundsSpinner = (Spinner) findViewById(R.id.num_rounds_spinner);
		this.titleTextView = (TextView) findViewById(R.id.bluetoothText);
		this.nameTextView = (TextView) findViewById(R.id.namePrompt);
		this.roundsTextView = (TextView) findViewById(R.id.numRoundsTextView);
		this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		initButtonListeners();
		SharedPreferences preferences = getPreferences(MODE_PRIVATE);
		this.enteredName.setText(preferences.getString(SAVED_NAME, ""));
		
		Typeface thin = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/roboto_thin.ttf");
        Typeface bold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/roboto_bold.ttf");
        
        this.titleTextView.setTypeface(bold);
        this.nameTextView.setTypeface(thin);
        this.roundsTextView.setTypeface(thin);
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		SharedPreferences preferences = this.getPreferences(MODE_PRIVATE);
		Editor edit = preferences.edit();
		edit.putString(SAVED_NAME, this.enteredName.getText().toString());
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	    List<String> rounds = Arrays.asList(getResources().getStringArray(R.array.num_rounds_string_array));
        List<String> time = Arrays.asList(getResources().getStringArray(R.array.time_limit_string_array));
        
	    this.enteredName.setText(SettingsModel.getPlayerName());
        this.numRoundsSpinner.setSelection(rounds.indexOf(SettingsModel.getRoundLimit() + ""));
		this.ensureDiscoverable();
	}

	private void initButtonListeners(){
		this.hostGameButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				String name = enteredName.getText().toString();
				SettingsModel.setPlayerName(name);
				
				Player.setDeviceHost(true); //set as device host
				Player.setGameHost(true); //device host is the first game host
				Player.setName(SettingsModel.getPlayerName()); //get players name
				Player.setHostRound(1); //hosting device will host round 1
				android.content.Intent intent = new android.content.Intent(getApplicationContext(), HostActivity.class);
				startActivityForResult(intent, REQUEST_CONNECT_DEVICE_SECURE);
			}
		});
		
		this.joinGameButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				String name = enteredName.getText().toString();
				SettingsModel.setPlayerName(name);
				
				Player.setDeviceHost(false);//not a device host
				Player.setGameHost(false); //not a game host
				Player.setName(SettingsModel.getPlayerName());//get players name
				
				android.content.Intent intent = new android.content.Intent(getApplicationContext(), JoinActivity.class);
				startActivityForResult(intent, REQUEST_CONNECT_DEVICE_SECURE);
			}
		});
		
		this.enteredName.setOnKeyListener(new OnKeyListener()
		{
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event)
			{
				if(keyCode == KeyEvent.KEYCODE_ENTER)
				{
					String name = enteredName.getText().toString();
					SettingsModel.setPlayerName(name);
					//Toast toast = Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT);
					//toast.show();
					InputMethodManager imm = (InputMethodManager)getSystemService(
						      Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(enteredName.getWindowToken(), 0);
					return true;
				}
				return false;
			}
		});
		
		this.numRoundsSpinner.setOnItemSelectedListener (new OnItemSelectedListener (){

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id)
			{
				String item = (String) parent.getItemAtPosition(position);
				SettingsModel.setRoundLimit(Integer.parseInt(item));
				//Toast toast = Toast.makeText(getApplicationContext(), item, Toast.LENGTH_SHORT);
				//toast.show();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0)
			{
				
			}
		});
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(D) Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
        case REQUEST_CONNECT_DEVICE_SECURE:
            // When DeviceListActivity returns with a device to connect
            if (resultCode == Activity.RESULT_OK) {
                //connectDevice(data, true);
            }
            break;
        case REQUEST_ENABLE_BT:
            // When the request to enable Bluetooth returns
            if (resultCode == Activity.RESULT_OK) {
                // Bluetooth is now enabled
                Log.d(TAG, "BT enabled");
                //Toast.makeText(this, R.string.bt_enabled, Toast.LENGTH_SHORT).show();
            } else {
                // User did not enable Bluetooth or an error occurred
                Log.d(TAG, "BT not enabled");
                Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.bluetooth, menu);
		return true;
	}
	
	private void ensureDiscoverable() {
        if(D) Log.d(TAG, "ensure discoverable");
        if (mBluetoothAdapter.getScanMode() !=
            BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }
	
	
    
	
    
	 
}
