package com.prototype.verticalprototype;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.cuetoclips.R;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerFragment;

public class YouTubePlayerDialogActivity extends YouTubeFailureRecoveryActivity {
    private String videoId;
    private String videoTitle;
    private String videoDescription;
    private String videoThumbnailUrl;
    private String videoChannelTitle;
    private int videoTimestamp;
    private boolean isGameHost;
    private ImageButton sendButton;
    private ImageButton backButton;
    private Context context;
    private YouTubePlayer player;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        this.videoId = (String) getIntent().getSerializableExtra("videoId");
        this.videoTitle = (String) getIntent().getSerializableExtra("videoTitle");
        this.videoDescription = (String) getIntent().getSerializableExtra("videoDescription");
        this.videoChannelTitle = (String) getIntent().getSerializableExtra("channelTitle");
        this.videoThumbnailUrl = (String) getIntent().getSerializableExtra("thumbnailUrl");
        try {
            this.videoTimestamp = Integer.parseInt((String) getIntent().getSerializableExtra("timestamp"));
        } catch (Exception e) {
            this.videoTimestamp = 0;
        }
        this.isGameHost = Boolean.parseBoolean((String) getIntent().getSerializableExtra("isGameHost"));
        
        this.context = this;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_dialog_youtube_player);
        
        WindowManager.LayoutParams windowManager = getWindow().getAttributes();
        windowManager.dimAmount = (float) 0.0;
        windowManager.width = LayoutParams.MATCH_PARENT;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        getWindow().setAttributes(windowManager);
        
        this.sendButton = (ImageButton) findViewById(R.id.button_airplane);
        this.backButton = (ImageButton) findViewById(R.id.button_back);
        initListeners();
        
        if (this.isGameHost) {
            this.sendButton.setVisibility(View.GONE);
        }

        YouTubePlayerFragment youTubePlayerFragment = 
                (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_fragment);
        youTubePlayerFragment.initialize(DeveloperKey.DEVELOPER_KEY, this);
    }
    
    private void initListeners() {
        this.sendButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(context, sendButton);
                popup.getMenuInflater().inflate(R.menu.menu_controls, popup.getMenu());
                
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        JSONObject jsonVideo = new JSONObject();
                        try {
                            jsonVideo.put("action", "VIDEO");
                            jsonVideo.put("videoId", videoId);
                            jsonVideo.put("videoTitle", videoTitle);
                            jsonVideo.put("channelTitle", videoChannelTitle);
                            jsonVideo.put("videoDescription", videoDescription);
                            jsonVideo.put("thumbnailUrl", videoThumbnailUrl);
                            jsonVideo.put("name", SettingsModel.getPlayerName());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Intent intent;
                        
                        switch (item.getItemId()) {
                        case R.id.control_item_submit_video:
                            try {
                                jsonVideo.put("videoTimestamp", "0");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            sendMessage(jsonVideo.toString());
                            
                            intent = new Intent(getApplicationContext(), RoundWaitingActivity.class);
                            startActivity(intent);
                            break;

                        case R.id.control_item_submit_video_from_time:
                            int currentTimeInMillis = player.getCurrentTimeMillis();
                            int currentTimeInSec = currentTimeInMillis / 1000;
                            
                            try {
                                jsonVideo.put("videoTimestamp", currentTimeInSec);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            sendMessage(jsonVideo.toString());
                            
                            intent = new Intent(getApplicationContext(), RoundWaitingActivity.class);
                            startActivity(intent);
                            break;
                            
                        default:
                            break;
                        }
                        return true;
                    }
                });
                
                popup.show();
            }
        });
        
        this.backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
        });
    }
    
    /**
     * Sends a message.
     * @param message  A string of text to send.
     */
    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (Chat.getChatService().getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            Chat.getChatService().broadcast(send);
        }
    }
    
    @Override
    public void onInitializationSuccess(Provider provider, YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {
            this.player = player;
            this.player.loadVideo(this.videoId, this.videoTimestamp * 1000);
        }
    }

    @Override
    protected Provider getYouTubePlayerProvider() {
        return (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_fragment);
    }

}
