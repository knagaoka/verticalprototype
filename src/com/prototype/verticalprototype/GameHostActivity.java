package com.prototype.verticalprototype;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.cuetoclips.R;

public class GameHostActivity extends Activity implements Observer {
	private TextView topicCardDisplay;
	private ListView videoReceivedList;
	private VideoListAdapter videoAdapter;	
	private ArrayList<Video> videoList;
	private int numReceived;
	private Context context = this;
	private String TAG = "GameHostActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_host);
		
		this.numReceived = 0;
		this.videoList = new ArrayList<Video>();
		this.videoAdapter = new VideoListAdapter(this, videoList);
		this.topicCardDisplay = (TextView) findViewById(R.id.topicCardHost);
		this.videoReceivedList = (ListView) findViewById(R.id.videoReceivedList);
		this.videoReceivedList.setAdapter(videoAdapter);

		initListeners();
		
		HandlerAdapter.getMessages().addObserver(this);
		
		if (savedInstanceState == null) {
		    String topic = Deck.getCardAtPosition((int)(Math.random() * ((Deck.getSize() - 1) + 1)));
	        this.topicCardDisplay.setText(topic);
	        Deck.removeCard(topic);
	        sendTopicCardSelection(topic);
		}
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    
	    String videos = "";
	    for (Video video : this.videoList) {
	        String vid = video.toJsonString();
	        Log.d("onsavedinstancestate", vid);
	        videos += vid;
	    }
	    
	    String topicCard = (String) this.topicCardDisplay.getText();
	    Log.d("fullString", topicCard);
	    Log.d("fullString", videos);
	    outState.putString("videos", videos);
	    outState.putString("topicCard", topicCard);
	}
	
	@Override
	public void onRestoreInstanceState(Bundle savedState) {
	    if (savedState != null && savedState.containsKey("videos") && savedState.containsKey("topicCard")) {
	        super.onRestoreInstanceState(savedState);
	        
	        this.numReceived = 0;
	        String[] videos = savedState.getString("videos").split("(?<=\\})(?=\\{)");
	        for (String vid : videos) {
	            Log.d("beta", vid);
	            this.receiveVideo(vid);
	        }
	        
	        String topicCard = savedState.getString("topicCard");
	        this.topicCardDisplay.setText(topicCard);
	    }
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		HandlerAdapter.getMessages().deleteObserver(this);
	}

	@Override
    public void onBackPressed() {
        // do nothing
    }
	
	public void receiveVideo(String videoInfo)
	{
		HashMap<String, String> metadata = parseJsonResults(videoInfo);
		Video video = new Video(metadata.get("videoId"),
				metadata.get("videoTitle"),
				metadata.get("channelTitle"),
				metadata.get("videoDescription"),
				metadata.get("thumbnailUrl"),
				metadata.get("videoTimestamp"),
				metadata.get("name"));
		
		videoList.add(video);
		
		numReceived++;
		Log.d(TAG, "Number of received videos: " + this.numReceived);
		if(numReceived == (GameState.getPlayers() - 1))
		{
			videoAdapter.notifyDataSetChanged();
		}
	}
	
	private void initListeners(){
	    
		this.videoReceivedList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			    String videoId = ((VideoView) view).getVideo().getVideoId();
                Video vid = ((VideoView) view).getVideo();
                
                Intent intent = new Intent(getApplicationContext(), YouTubePlayerDialogActivity.class);
                intent.putExtra("videoId", videoId);
                intent.putExtra("videoTitle", vid.getVideoTitle());
                intent.putExtra("channelTitle", vid.getVideoChannel());
                intent.putExtra("videoDescription", vid.getVideoDescription());
                intent.putExtra("thumbnailUrl", vid.getVideoThumbnailUrl());
                intent.putExtra("timestamp", vid.getTimestamp());
                intent.putExtra("isGameHost", "true");
                startActivity(intent);
			}			
		});
		
		this.videoReceivedList.setOnItemLongClickListener(new OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_choose_video);
                ImageButton dialogOkay = (ImageButton) dialog.findViewById(R.id.dialog_button_submit_okay);
                ImageButton dialogCancel = (ImageButton) dialog.findViewById(R.id.dialog_button_submit_cancel);
                
                dialogOkay.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        
                	    JSONObject jObjectData = new JSONObject();
                	    try
                		{
                		    jObjectData.put("action", "VIDEO_SELECT");
                		    jObjectData.put("videoTitle", videoList.get(position).getVideoTitle());
                		    jObjectData.put("roundWinner", videoList.get(position).getPlayerName());
                		    jObjectData.put("gameOver", "false");
                		    jObjectData.put("gameWinner", "null");
                		}catch(JSONException e)
                		{
                			e.printStackTrace();
                		}

                	    String chosenVideo = jObjectData.toString();
                	    
                	    // set the winner of the round
                	    GameState.setWinnerOfLastRound(videoList.get(position).getPlayerName());
                	    
                	    Intent intent;
                	    if(GameState.getCurrentRound() != SettingsModel.getRoundLimit())
            		    {
                	    	intent = new Intent(getApplicationContext(), RoundOverActivity.class);
            		    }
            		    else
            		    {
            		    	intent = new Intent(getApplicationContext(), GameOverActivity.class);
            		    }
                	    
                        
                        startActivity(intent);
                        
                        sendMessage(chosenVideo);
                        finish();
                    }
                });
                dialogCancel.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("vertproto", "clicked cancel");
                        dialog.dismiss();
                    }
                });
                
                dialog.show();
                
                return true;
            }
		});
	}
	
	/**
	 * Parses the JSON object that is returned from a bluetooth client
	 * 
	 * @param results the stringified JSON object
	 * @return hashmap with metadata
	 */
	private HashMap<String, String> parseJsonResults(String results) {
		HashMap<String, String> video = new HashMap<String, String>();

		Log.d("jsonresults", results);
		
		try {
			JSONObject jObject = new JSONObject(results);
			video.put("videoId", jObject.getString("videoId"));
			video.put("videoTitle", jObject.getString("videoTitle"));
			video.put("videoDescription", jObject.getString("videoDescription"));
			video.put("channelTitle", jObject.getString("channelTitle"));
			video.put("videoTimestamp", jObject.getString("videoTimestamp"));
			video.put("thumbnailUrl", jObject.getString("thumbnailUrl"));
			video.put("name", jObject.getString("name"));
			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return video;
	}

    /**
     * Sends a message.
     * @param message A string of text to send.
     */
    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (Chat.getChatService().getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothService to write
            byte[] send = message.getBytes();
            Chat.getChatService().broadcast(send);
        }
    }
	
    @Override
    public void update(Observable observable, Object data) {
        String message = HandlerAdapter.getMessages().getMessage();
        Log.d(TAG, "Message received: " + message);
        try {
            JSONObject jObject = new JSONObject(message);
            String action = jObject.getString("action");
            
            if (action.equals("VIDEO")) {
                receiveVideo(message);
            }
            else if(action.equals("SCORE"))
            {
            	Log.d("SCORE", "SCORE RECEIVED");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendTopicCardSelection(String topicCard) {
        JSONObject jObjectData = new JSONObject();
        try {
            jObjectData.put("action", "TOPIC_CARD");
            jObjectData.put("topic", topicCard);
            sendMessage(jObjectData.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        } 
    }
}
