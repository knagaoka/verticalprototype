package com.prototype.verticalprototype;

import java.util.Observable;
import java.util.Observer;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.cuetoclips.R;

public class GameOverActivity extends Activity implements Observer{

	private TextView roundWinner;
	private TextView gameWinner;
	private Button gameOver;
	private ListView scoreListView;
	//private ArrayList<Score> scoreList = new ArrayList<Score>();
	private ScoreListAdapter scoreListAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_over);
		this.scoreListView = (ListView)findViewById(R.id.gameOverInnerLayout);
		this.roundWinner = (TextView) findViewById(R.id.roundWinnerText);
		this.gameWinner = (TextView) findViewById(R.id.gameWinnerText);
		this.gameOver = (Button) findViewById(R.id.gameOverButton);
		
		initListeners();
    	this.scoreListAdapter = new ScoreListAdapter(this, GameState.getScoreList());
    	this.scoreListView.setAdapter(this.scoreListAdapter);
		
		HandlerAdapter.getMessages().deleteObservers();
		HandlerAdapter.getMessages().addObserver(this);
		
		this.scoreListAdapter.notifyDataSetChanged();
		
		Log.d("GameOverActivity OnCreate", "New GameOverActivity Created");
		//send your score to everyone
		sendScore();
    	if(Player.getDeviceHost())
    	{
    		Log.d("GameOverActivity OnCreate DH",
					Player.getName() + " = "
					+ Player.getRoundsWon());
    		addNewView(Player.getName(), Player.getRoundsWon() + "");
    	}
    	
    	// Set the text for the winner of the last round
		this.roundWinner.setText(GameState.getWinnerOfLastRound()
					+ " wins Round "
					+ (GameState.getCurrentRound()));
		this.roundWinner.setTextSize(36);
		
		Typeface thin = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/roboto_thin.ttf");
		this.roundWinner.setTypeface(thin);
		this.gameWinner.setTypeface(thin);
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		//this.scoreList.clear();
		//HandlerAdapter.getMessages().deleteObserver(this);
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		HandlerAdapter.getMessages().deleteObserver(this);
	}
	
	@Override
    public void onBackPressed() {
        // do nothing
    }
	
	private void addNewView(String playerName, String score)
	{
		Log.d("Adding New Score View", "player: " + playerName + ", score: " + score);
		//create a new view to add a new score
		Score newScore = new Score(playerName, score);
		GameState.getScoreList().add(newScore);
		//this.scoreList.add(newScore);
		//ScoreView scoreView = new ScoreView(this, newScore);
		this.scoreListAdapter.notifyDataSetChanged();
		//this.scoreListView.addView(scoreView);
		//setContentView(this.topLayout);
		
		if (GameState.getScoreList().size() == GameState.getPlayers()) {
			int highScore = 0;
			String highPlayer = "";
			
			for (Score s: GameState.getScoreList()) {
				String player = s.getPlayerName();
				int scr = Integer.parseInt(s.getScore());
				
				// WHAT HAPPENS IF THERES MORE THAN ONE WINNER (TIE) ?
				if (scr > highScore) {
					highScore = scr;
					highPlayer = player;
				}
			}
			
			this.gameWinner.setText("WINNER: " + highPlayer);
		}
	}
	
	private void sendScore()
	{
	    JSONObject jObjectData = new JSONObject();
	    try
		{
		    jObjectData.put("action", "SCORE");
		    jObjectData.put("playerName", Player.getName());
		    jObjectData.put("score", Player.getRoundsWon());
		}catch(JSONException e)
		{
			e.printStackTrace();
		}
	    String scoreMsg = jObjectData.toString();
        sendMessage(scoreMsg);
	}
	
	private void initListeners(){
		this.gameOver.setOnClickListener(new OnClickListener()
		{
			//all players should be able to click the agme over button
			@Override
			public void onClick(View v)
			{
				// need to reset all the model data here (GameState and Settings? and Player)
				// either that or we can just make sure we set all the defaults in the beginning of a game
        	    GameState.reset();
        	    Player.reset();
        	    Chat.reset();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
			}
		});
	}

    @Override
    public void update(Observable observable, Object data) {
        // get message from MessageDump and parse
        String jsonRoundMessage = HandlerAdapter.getMessages().getMessage();
        Log.d("GameOverActivity", "Whole message received: " + jsonRoundMessage);
        String[] strs = jsonRoundMessage.split("(?<=\\})(?=\\{)");
		for(String s : strs)
		{
			try
			{
				JSONObject jObject = new JSONObject(s);
				String action = jObject.getString("action");
				Log.d("GAMEOVERACTIVITY", s);
				if(action.equals("SCORE"))
				{
					Log.d("UPDATE SCORE", jObject.getString("playerName") + " = "
							+ jObject.getString("score"));
					addNewView(jObject.getString("playerName"),
							jObject.getString("score"));
				}
				
			}catch(JSONException e)
			{
				e.printStackTrace();
			}
		}
    }
    
    /**
     * Sends a message.
     * @param message A string of text to send.
     */
    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (Chat.getChatService().getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            Chat.getChatService().broadcast(send);
        }
    }

}
