package com.prototype.verticalprototype;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.cuetoclips.R;

public class JoinActivity extends Activity implements Observer {

	// Debugging
    private static final String TAG = "JoinActivity";
    private static final boolean D = true;
    
	// Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    
    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
    
    // Intent request codes
    private static final int REQUEST_ENABLE_BT = 3;
    
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    
    private TextView mStatusView;
    
 // Return Intent extra
    public static String EXTRA_DEVICE_ADDRESS = "device_address";
    
    private TimerTask timerTask;
    private Timer timer;
    private ArrayList<Integer> loadingBarList;
    private int currentLoadingBarImage = 1;
    private ImageView loadingBar;
    private static int NUM_LOAD_IMAGES = 8;
    private static int INTERVAL_TIME = 500;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_join);
		
		this.mStatusView = (TextView) findViewById(R.id.join_status);
		// Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		
		String deviceName = mBluetoothAdapter.getName();
        if (deviceName.contains("alpha")) {
        	mBluetoothAdapter.setName(deviceName.replace("-alpha", ""));
        }
        
		// Set result CANCELED in case the user backs out
        setResult(Activity.RESULT_CANCELED);

        // initialize the deck
        Resources res = getResources();
        Deck.setCards(res.getStringArray(R.array.topic_card_strings));

        // Register for broadcasts when a device is connected
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED);
        this.registerReceiver(mReceiver, filter);
        
        // Register for broadcasts when a device is disconnected
        filter = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        this.registerReceiver(mReceiver, filter);
        
        // Register for broadcasts when a device is discovered
        filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(mReceiver, filter);
        
        HandlerAdapter.getMessages().addObserver(this);
        Chat.setUpNewChat(new HandlerAdapter(this));

        
		//load all the image ids to a list
		this.loadingBarList = new ArrayList<Integer>();
		this.loadingBarList.add(R.drawable.loading_0);
		this.loadingBarList.add(R.drawable.loading_1);
		this.loadingBarList.add(R.drawable.loading_2);
		this.loadingBarList.add(R.drawable.loading_3);
		this.loadingBarList.add(R.drawable.loading_4);
		this.loadingBarList.add(R.drawable.loading_5);
		this.loadingBarList.add(R.drawable.loading_6);
		this.loadingBarList.add(R.drawable.loading_7);
		this.loadingBarList.add(R.drawable.loading_8);
        //loading bar task and timer
		this.loadingBar = (ImageView)findViewById(R.id.loadingBar);
        this.timer = new Timer();
		this.timerTask = new TimerTask()
		{
			@Override
			public void run() {
				//needs to run on the UI Thread
				runOnUiThread(new Runnable()
				{
					@Override
					public void run()
					{
						changeLoadingBar();							
					}
				});
			}
			
		};
		this.timer.scheduleAtFixedRate(this.timerTask, 0, INTERVAL_TIME);

        Chat.setJoinActivity(this);

        Typeface thin = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/roboto_thin.ttf");
        this.mStatusView.setTypeface(thin);
    }
	
	private void changeLoadingBar()
	{
		int loadNum = currentLoadingBarImage % NUM_LOAD_IMAGES;
		//full loading bar case
		if(loadNum == 0)
			loadNum = NUM_LOAD_IMAGES;
		this.loadingBar.setImageResource(this.loadingBarList.get(loadNum));
		currentLoadingBarImage++;
	}
	
	@Override
    public void onStart() {
        super.onStart();
        if(D) Log.e(TAG, "++ ON START ++");
        // If BT is not on, request that it be enabled.
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }
    }
	
	@Override
	protected void onResume() {
		if(D) Log.e(TAG, "+ ON RESUME +");
		super.onResume();
		// Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (Chat.getChatService() != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (Chat.getChatService().getState() == BluetoothService.STATE_NONE) {
              // Start the Bluetooth chat services
            	Chat.getChatService().start();
            }
        }
        
        doDiscovery();
	}
	
	@Override
    protected void onDestroy() {
        super.onDestroy();

        if (Chat.getChatService() != null) Chat.getChatService().stop();
        if(D) Log.e(TAG, "--- ON DESTROY ---");
        
        // Make sure we're not doing discovery anymore
        if (mBluetoothAdapter != null) {
        	mBluetoothAdapter.cancelDiscovery();
        }

        // Unregister broadcast listeners
        this.unregisterReceiver(mReceiver);
        this.timerTask.cancel();
        
        HandlerAdapter.getMessages().deleteObserver(this);
    }
	
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        HandlerAdapter.getMessages().deleteObserver(this);
        finish();
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.join, menu);
		return true;
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.discoverable:
            // Ensure this device is discoverable by others
            ensureDiscoverable();
            return true;
        }
        return false;
    }
	
    private void ensureDiscoverable() {
        if(D) Log.d(TAG, "ensure discoverable");
        if (mBluetoothAdapter.getScanMode() !=
            BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }
    
    /**
     * Start device discover with the BluetoothAdapter
     */
    private void doDiscovery() {
        if (D) Log.d(TAG, "doDiscovery()");

        // Indicate scanning in the title
        setProgressBarIndeterminateVisibility(true);
        //setTitle(R.string.scanning);

        // Turn on sub-title for new devices
        //findViewById(R.id.title_new_devices).setVisibility(View.VISIBLE);

        // If we're already discovering, stop it
        if (mBluetoothAdapter.isDiscovering()) {
        	mBluetoothAdapter.cancelDiscovery();
        }

        // Request discover from BluetoothAdapter
        mBluetoothAdapter.startDiscovery();
    }
    
    /**
     * Sends a message.
     * @param message A string of text to send.
     */
    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (Chat.getChatService().getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            Chat.getChatService().broadcast(send);
        }
    }
    
 // The BroadcastReceiver that listens for discovered devices and
    // changes the title when discovery is finished
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
            	String connected = getResources().getString(R.string.join_status_connected);
        		mStatusView.setText(connected);
        		//get the full bar
        		loadingBar.setImageResource(loadingBarList.get(loadingBarList.size() - 1));
        		//kill the timertask
        		timerTask.cancel();
            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
            	String connected = getResources().getString(R.string.join_status_connecting);
        		mStatusView.setText(connected);
        		//show an empty bar
        		loadingBar.setImageResource(loadingBarList.get(0));
        		//start loading animation again
        		timerTask.run();
            }
            
            
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // If it's already paired, skip it, because it's been listed already
                Log.d(TAG, "Found Device name: " + device.getName());
                String deviceName = device.getName();
            	if (deviceName.contains("alpha")) {
            		mBluetoothAdapter.cancelDiscovery();
                	Chat.getChatService().connect(device);                	
            	}
            } 
        }
    };

    @Override
    public void update(Observable observable, Object data) {
        // get message from MessageDump and parse
        String jsonRoundMessage = HandlerAdapter.getMessages().getMessage();
        Log.d(TAG, "Join Activity update called");
        try {
            JSONObject jObject = new JSONObject(jsonRoundMessage);
            String action = jObject.getString("action");
            
            if (action.equals("ROUND_SETUP") && Player.getHostRound() == 0) {
                Player.setHostRound(Integer.parseInt(jObject.getString("round")));
                Log.d(TAG, "Set Host Round: " + Player.getHostRound());
                //Toast.makeText(this, "Round: " + Player.getHostRound(), Toast.LENGTH_SHORT).show();
            }
            else if (action.equals("NUMBER_OF_PLAYERS")) {
            	GameState.setPlayers(Integer.parseInt(jObject.getString("players")));
            }
            else if (action.equals("TOPIC_CARD")) {
            	String topic = jObject.getString("topic");
            	GameState.setTopic(topic);
            	Deck.removeCard(topic);
            }
            else if (action.equals("START_GAME")) {
            	int roundLimit = Integer.parseInt(jObject.getString("roundLimit"));
            	SettingsModel.setRoundLimit(roundLimit);
            	android.content.Intent intent = new android.content.Intent(getApplicationContext(), GamePlayerActivity.class);
				startActivity(intent);
				//HandlerAdapter.getMessages().deleteObserver(this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
