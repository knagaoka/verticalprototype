package com.prototype.verticalprototype;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Deck {
    private static List<String> cards;
    
    public static void setCards(String[] cardResources) {
        cards = new LinkedList<String>(Arrays.asList(cardResources));
    }
    
    /**
     * Returns the deck of cards.
     * @return list of cards
     */
    public static List<String> getCards() {
        return cards;
    }
    
    /**
     * Removes the given card from the deck.
     * @param card
     */
    public static void removeCard(String card) {
        cards.remove(card);
    }
    
    /**
     * Returns the size of the deck.
     * @return the size of the deck
     */
    public static int getSize() {
        return cards.size();
    }
    
    /**
     * Returns the card from the deck at the selected position.
     * @param position
     * @return card at the given position
     */
    public static String getCardAtPosition(int position) {
        return cards.get(position);
    }
}
