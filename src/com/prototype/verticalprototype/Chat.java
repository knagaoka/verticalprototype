package com.prototype.verticalprototype;

import android.app.Activity;
import android.os.Handler;

public class Chat{

	private static Handler mHandler = null;
	private static BluetoothService mBtService = null;
	private static Activity mHostActivity;
	private static Activity mJoinActivity;

	public static Handler getHandler() {
		return mHandler;
	}
	
	public static void setUpNewChat(Handler handler) {
		mHandler = handler;
		mBtService = new BluetoothService(mHandler);
	}
	
	public static BluetoothService getChatService() {
		return mBtService;
	}
	
	public static void setHostActivity(Activity hostActivity) {
		Chat.mHostActivity = hostActivity;
	}
	
	public static void setJoinActivity(Activity joinActivity) {
		Chat.mJoinActivity = joinActivity;
	}
	
	public static void reset() {
		mBtService.stop();
		mHandler = null;
		mBtService = null;
		if (mHostActivity != null) {
			mHostActivity.finish();
		}
		if (mJoinActivity != null){
			mJoinActivity.finish();
		}
		mHostActivity = null;
		mJoinActivity = null;
	}
}
