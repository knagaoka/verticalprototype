package com.prototype.verticalprototype;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Typeface;
import android.view.Menu;
import android.widget.TextView;
import com.cuetoclips.R;

public class CreditsActivity extends Activity
{
	private TextView creditsTitle, developersTitle, graphicArtistTitle;
	private TextView dev1, dev2, dev3, artist1;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_credits);
		
		creditsTitle = (TextView) findViewById(R.id.creditsTitle);
		developersTitle = (TextView) findViewById(R.id.developerText);
		graphicArtistTitle = (TextView) findViewById(R.id.graphicArtistText);
		dev1 = (TextView)findViewById(R.id.dev1);
		dev2 = (TextView)findViewById(R.id.dev2);
		dev3 = (TextView)findViewById(R.id.dev3);
		artist1 = (TextView)findViewById(R.id.artist1);
		
		Typeface regular = Typeface.createFromAsset(this.getAssets(), "fonts/roboto_regular.ttf");
		Typeface bold = Typeface.createFromAsset(this.getAssets(), "fonts/roboto_bold.ttf");
		this.creditsTitle.setTypeface(bold);
		this.developersTitle.setTypeface(bold);
		this.graphicArtistTitle.setTypeface(bold);
		this.dev1.setTypeface(regular);
		this.dev2.setTypeface(regular);
		this.dev3.setTypeface(regular);
		this.artist1.setTypeface(regular);
	}

}
