package com.prototype.verticalprototype;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ScoreView extends LinearLayout
{
	private Context context;
	private Score score;
	private TextView nameText, scoreText;
	public ScoreView(Context context, Score score)
	{
		super(context);
		this.context = context;
		this.score = score;
		initLayout();
	}
	
	private void initLayout()
	{
		this.setOrientation(HORIZONTAL);
		nameText = new TextView(context);
		nameText.setText(this.score.getPlayerName());
		nameText.setTextSize(28);
		//set weight to .75
		nameText.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, (float) .75));
		this.addView(nameText);
		//set gravity to right
		scoreText = new TextView(context);
		scoreText.setText(this.score.getScore());
		scoreText.setTextSize(28);
		scoreText.setGravity(Gravity.RIGHT);
		//setContentView();
		this.addView(scoreText);
		
		Typeface thin = Typeface.createFromAsset(this.context.getAssets(), "fonts/roboto_thin.ttf");
		this.nameText.setTypeface(thin);
		this.scoreText.setTypeface(thin);
	}
	
	public void setScore(Score newScore)
	{
		this.score = newScore;
		this.nameText.setText(newScore.getPlayerName());
		this.scoreText.setText(newScore.getScore());
	}
}
